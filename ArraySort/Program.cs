﻿using System;
using System.Text;
using System.IO;


namespace ArraySort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] Arr = new int[] { 105, 1, 34, 888, 7, 1};
            Sort(Arr);
            Console.WriteLine(string.Join(", ", Arr));
            Console.ReadLine();
        }

        static void Sort(int[] IntArray)
        {
            for (int ind = 0; ind < IntArray.Length; ind ++)
            {
                int startIndex = ind;//начальный индекс

                int TempValue = 0;//хранит текущее массива

                for (int i = startIndex + 1; i < IntArray.Length; i++)//так мы находим минимальное значение из всех в массиве
                {
                    //IntArray[startIndex] в каждом цикле хранит минимальное значение
                    TempValue = IntArray[i];//сохраняем текущее значение
                    if (IntArray[startIndex] > TempValue)//если  наше минимальное значение вдруг больше текущего
                    {
                        //меняем их местами
                        IntArray[i] = IntArray[startIndex];
                        IntArray[startIndex] = TempValue;

                    }
                }
                
            }
        }
    }
}
